<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_4_5_9');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'wR}+4FUlCz<++hZ,m:GHq}&a}grp~X|; ;/aFd|3;5SMej^%6I22fI#yI| is;,L');
define('SECURE_AUTH_KEY',  '^Z,~)6J:kjRq^wYP:F^K<P,N8Yk%SOr-@N9b+CpMo[Jy8~|VXJ=c+*|yi]):|Pwy');
define('LOGGED_IN_KEY',    'pvNe+To{Lmf6qm6~kL~X7_y7$URAO?Y;|>wb[?Tk(X8YP2rz)Ak]f9!g(kI*=Y[1');
define('NONCE_KEY',        '8+1g<=DX WkZG1A,rs..*)ktfYdyjg_3R.?d2}!|IF_Yr#xa@Qv9N ^A&,wc<q{l');
define('AUTH_SALT',        'oLp3Gp~uBE2U`~P8bzy(A|j`[M8xY*223U.D%U5ZK:[?d8;,m@G$>mdC!-wmZ<v4');
define('SECURE_AUTH_SALT', '2:T-A>|I|Xt`JHfaxSZuuV|s:iM=AK$ZC%E39rsgXYLsm[TJW`XrLoJaV*.no:X{');
define('LOGGED_IN_SALT',   'ndHmup/(zw<#|*i[.k}4Z_2D>|c>`=Kz~YUp|12Uf_i7rg+WXg7aK7QM|H3l^}uU');
define('NONCE_SALT',       '*U:4|a`>n}xgR*SNA#%m](nh,njAWui=.8U)w]{[corhL2%$w5gyIK4Pt?tI,LKL');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
