<?php $options = get_option('klaus'); ?>
</div>
<!-- End Main -->

<?php if( !empty($options['enable-back-to-top']) && $options['enable-back-to-top'] == 1) { ?>
<!-- Back To Top -->
<a id="back-to-top" href="#">
    <i class="font-icon-arrow-up-simple-thin"></i>
</a>
<!-- End Back to Top -->
<?php } ?>

<footer>
<?php if ( is_home() || is_search() || is_404() ) {
	// Blog Page and Search Page
	az_footer_widget(get_option('page_for_posts'));
}
else {
	// All Other Pages and Posts
	az_footer_widget(get_the_ID());
}
?>

<!-- Start Footer Credits -->
<section id="footer-credits">
	<div class="container">
		<div class="row">

			
            
		</div>
	</div>
</section>
<!-- Start Footer Credits -->

</footer>

</div>
<!-- End Wrap All -->


<?php if(!empty($options['tracking-code'])) echo $options['tracking-code']; ?> 

<?php 	
	wp_register_script('main', get_template_directory_uri() . '/_include/js/main.js', array('jquery'), NULL, true);
	wp_enqueue_script('main');
	
	wp_localize_script(
		'main', 
		'theme_objects',
		array(
			'base' => get_template_directory_uri()
		)
	);

	wp_footer();  
?>	

</body>
</html>